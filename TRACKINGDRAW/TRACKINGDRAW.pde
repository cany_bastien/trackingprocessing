import processing.video.*;

Capture video;

import processing.sound.*;
Amplitude amp;
AudioIn in;


color trackColor;

float threshold = 25;

void setup(){
  frameRate(30);
  size (640, 360);
  String[] cameras = Capture.list();
  printArray(cameras);
  video = new Capture(this, cameras[3]);
  video.start();
  trackColor = color(255, 0, 0);
  
        // Create an Input stream which is routed into the Amplitude analyzer
      amp = new Amplitude(this);
      in = new AudioIn(this, 0);
      in.start();
      amp.input(in);
}

void captureEvent(Capture video) {
  video.read();
}

void draw() {
  //video.loadPixels();
  //image(video, 0, 0, width, height);
  
  float avgX = 0;
  float avgY = 0;
  
  int count = 0;
  
    // Begin loop to walk through every pixel
  for (int x = 0; x < video.width; x++ ) {
    for (int y = 0; y < video.height; y++ ) {
      int loc = x + y * video.width;
      // What is current color
      color currentColor = video.pixels[loc];
      float r1 = red(currentColor);
      float g1 = green(currentColor);
      float b1 = blue(currentColor);
      float r2 = red(trackColor);
      float g2 = green(trackColor);
      float b2 = blue(trackColor);

      float d = dist(r1, g1, b1, r2, g2, b2); 
         
      if (d < threshold) {
           avgX += x;
           avgY += y;
           count++;
          }
        }
  } 
  if (count > 5){
    avgX = avgX/count;
    avgY = avgY/count;
    fill(trackColor);
    strokeWeight(1.0);
    stroke(0);
    rect(10, 10, 20, 20);
    fill(255);
    ellipse(avgX, avgY, amp.analyze()*1000, amp.analyze()*1000);
  }
}

void mousePressed() {
  // Save color where the mouse is clicked in trackColor variable
  int loc = mouseX + mouseY*video.width;
  trackColor = video.pixels[loc];
}

void keyPressed() {
  if (key == 'x'){
  background(0);
  fill(255);
  }
  if (key == 'c'){
    saveFrame();
    println("image saved");
  }
}
