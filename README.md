# LIVRE PROTOCOLE

[PROJET](https://www.notion.so/PROJET-7c2a8cc37908402d8bfbad708e80675f)

---

- [x]  regarder des idées de interactive design pour nouvel idée de performance/dessin.
- [ ]  Trouver un titre
- [x]  Finir la page projet final (y inclure les recherche dans la partie "son random"

---

- [ ]  continuer de mettre en place des formes dans le programme (processing)

---

- [x]  Set up le fichier de la notice sur InDesign
- [ ]  Réfléchir à un protocole graphique (lié au sujet de la performance)
- [ ]  Commencer à sectionner la notice en plusieurs partie
- [ ]  Réfléchir au étapes de la notice
- [ ]  Ecrire un texte explicatif du projet

---

- [ ]  faire un github pour pouvoir accéder au code
- [ ]  designer un read me.

---

- [ ]  demander jean-no : agrandire fenetre proccessing + smooth edge